> # `gitlab-webhooks-notifications`
>
> A function that receives Webhooks from Gitlab, transforms them, and sends them to Discord.
>
> Related files : [[`function.js`](function.js)].
