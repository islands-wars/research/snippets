const req = require('request-promise-native')
const md5 = require('md5')

const generate_embeds = {
  'Push Hook' (body) {
    // TODO: Watch Issue #2 on auth0/webtasks cause destructuring doesn't work.
    return body.commits.reverse().map(commit => ((commit, body) => {
      const project = body.project
      const branch = body.ref.split('/').pop()
      return {
        title: project.path_with_namespace,
        description: `Committed on [${branch}](${project.homepage}/tree/${branch}).\n${commit.message}`,
        url: commit.url,
        timestamp: commit.timestamp,
        color: 2201331,
        author: {
          name: commit.author.name,
          icon_url: `https://www.gravatar.com/avatar/${md5(commit.author.email.trim().toLowerCase())}`
        },
        thumbnail: {
          url: project.avatar_url
        },
        footer: {
          text: 'Push event!',
          icon_url: 'https://github.com/webdog/octicons-png/blob/master/repo-push.svg.png?raw=true'
        },
        fields: [
          {
            "name": "📜 Files informations",
            "value": `There are ${commit.added.length} files added, ${commit.modified.length} modified and ${commit.removed.length} deleted`
          }
        ],
        image: {
          url: [
            'https://media.giphy.com/media/AbDb2PniluFwY/giphy.gif',
            'https://media1.tenor.com/images/4a1a5e2c2bb61fdaeaf2e0b912fe9fe5/tenor.gif',
            'https://media1.tenor.com/images/4c147c241198aafdeedde65d4b78d0f6/tenor.gif'
          ][~~(Math.random()*3)]
        }
      }
    })(commit, body))
  },
  'Tag Push Hook' (body) {
    const project = body.project
    const version = body.ref.split('/').pop()
    console.log(body.commits)
    return [{
      title: project.path_with_namespace,
      description: `New tag [${version}](${project.homepage}/tags/${version}).\n${project.description}`,
      url: `${project.homepage}/tags/${version}`,
      color: 4149685,
      author: {
        name: 'Yeah! New Tag pushed!',
        icon_url: 'https://i.imgur.com/KWdyf1J.png'
      },
      thumbnail: {
        url: project.avatar_url
      },
      footer: {
        text: 'New Tag event!',
        icon_url: 'https://github.com/webdog/octicons-png/blob/master/tag.svg.png?raw=true'
      },
      image: {
        url: [
          'https://media.giphy.com/media/xT9DPwKoIuCCHBdoeQ/giphy.gif'
        ][~~(Math.random()*1)]
      }
    }]
  }
}

const options = (url, kind, body) => {
  return {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
      'User-Agent': 'DiscordBot (islandswars.fr, 0.0.1)'
    },
    url,
    body: {
      embeds: generate_embeds[kind](body)
    },
    json: true
  }
}

/**
 * @param context {WebtaskContext}
 */
module.exports = function(context, cb) {
  const url = `https://discordapp.com/api/webhooks/${context.secrets.webhook_id}/${context.secrets.webhook_token}`

  if (context.headers['x-gitlab-token'] != context.secrets.secret_token) {
    return cb(null, {
      code: 403,
      error: "Access forbidden!",
      details: "Token in 'x-gitlab-token' doesn't allow you to access to this resource."
    })
  }

  console.log(context.headers['x-gitlab-event'])
  req(options(url, context.headers['x-gitlab-event'], context.body))
    .then(body => cb(null, 'Works!'))
    .catch(error => cb(null, 'An error occured'))
}

