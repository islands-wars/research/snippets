# <img src="https://islands-wars.gitlab.io/assets/brands/icons/icon.svg" width="64"> Snippets


> Islands Wars is a Minecraft sky block server.

> List all project snippets.

---

# Snippets

A whole bunch of codes related to the Islands Wars project.

---

> ### [`gitlab-webhooks-notifications`](gitlab-webhooks-notifications/)
>
> A function that receives Webhooks from Gitlab, transforms them, and sends them to Discord.

# License

---

> Do What The Fuck You Want to Public License
